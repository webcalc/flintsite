import datetime
from kotti.resources import Content,Document
from kotti import DBSession
from kotti.security import has_permission
from sqlalchemy.sql import or_,not_,and_
from kotti.views.util import TemplateAPI
from kotti.resources import get_root
from pyramid.view import view_config, view_defaults
from kotti_mysite.resources import Slider, Quote,QuoteRoot
from kotti_blog.resources import BlogEntry
from kotti_calendar.resources import Event

class BaseView(object):
    """ BaseView provides a common constructor method. """

    def __init__(self, context, request):
        """ Constructor.  Sets context and request as instance attributes.

        :param context: context.
        :type context: kotti.resources.Content or subclass thereof.

        :param request: request.
        :type request: pyramid.request.Request
        """

        self.context = context
        self.request = request

@view_defaults(context=QuoteRoot,permission = "view")
class QuotePageView(BaseView):

    @view_config(name="view",renderer = "kotti_mysite:templates/quoteroot.pt")
    def root(self):
        quotes = DBSession.query(Quote).filter(Quote.parent_id==self.context.id).\
            order_by(Quote.creation_date.desc()).all()
        items = [item for item in quotes if has_permission('view',item,self.request) ]
        return dict(items = items)

    @view_config(context=Quote,renderer="kotti_mysite:templates/quote.pt")
    def quote(self):
        return dict()



def related_items(context,request):
    searchstring = u"*%s*"%context.title
    q = or_(Content.name.ilike(searchstring),
            Content.title.ilike(searchstring))
    items = DBSession.query(Content).filter(q).\
        filter(not_(Content.type.in_(['image','file']))).\
        filter(Content.id!=context.id).all()


    api = TemplateAPI(context,request)
    return dict(items=[item for item in items
    if has_permission('view',item,request)],api=api)


@view_config(name='front-page',
        renderer='kotti_mysite:templates/front-page.pt',permission='view')
def front_page(request):
    quotes = DBSession.query(Quote).order_by(Quote.creation_date.desc()).limit(10).all()
    quotes = [item for item in quotes if has_permission('view',item, request)]
    pictures = DBSession.query(Slider).filter(Slider.title==u'Home Slider').first()
    if pictures:
        pictures = pictures.children_with_permission(request)
    else:
        pictures = None
    q = DBSession.query(QuoteRoot).first()
    about = DBSession.query(Document).filter(Document.name==u"about").first()
    latest_blog = DBSession.query(BlogEntry).order_by(BlogEntry.date.desc()).limit(10).all()
    latest_blog = [item for item in latest_blog if has_permission('view',item, request)]
    now = datetime.datetime.now()

    events = Event.query \
        .filter(or_(Event.start >= now, Event.end > now)) \
        .order_by(Event.start) \
        .all()
    events = [event for event in events if has_permission('view', event, request)]

    return dict(pictures=pictures,quotes=quotes,Qurl = q,about=about,latest_blog=latest_blog, events=events)

@view_config(name="fbt",
             renderer = "kotti_mysite:templates/fbt.pt")
def fbt(request):
    return dict()

def populate():

    site = get_root()
    site.default_view = 'front-page'
