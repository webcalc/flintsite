import colander
import cgi
from kotti.views.edit import ContentSchema
from kotti.views.edit.content import ImageAddForm,ImageEditForm
from kotti.views.form import AddFormView
from kotti.views.form import EditFormView
from pyramid.view import view_config
from colander import SchemaNode
from kotti.views.edit.content import FileData, FileUploadTempStore,validate_file_size_limit,FileUploadWidget,\
    FileEditForm, FileAddForm
from kotti.util import _
from kotti_mysite.resources import Slider, SliderImage, QuoteRoot, Quote


def FileSchema(tmpstore, title_missing=None):
    class FileSchema(ContentSchema):
        file = SchemaNode(
            FileData(),
            title=_(u'File'),
            widget=FileUploadWidget(tmpstore),
            validator=validate_file_size_limit,
            )
        link = SchemaNode(
            colander.String(),
            title = _(u"Link"),
            missing = colander.null,
        )

    def set_title_missing(node, kw):
        if title_missing is not None:
            node['title'].missing = title_missing

    return FileSchema(after_bind=set_title_missing)

class QuoteSchema(ContentSchema):
    title = colander.SchemaNode(
        colander.String(),
        title=u"Source"
    )
    description = colander.SchemaNode(
        colander.String(),
        title = u"Quote"
    )


@view_config(name=Slider.type_info.add_view, permission='add',
             renderer='kotti:templates/edit/node.pt')
class SliderAddform(AddFormView):
    schema_factory = ContentSchema
    add = Slider
    item_type = _(u'Slider')


@view_config(name='edit', context=Slider, permission='edit',
             renderer='kotti:templates/edit/node.pt')
class SliderEditForm(EditFormView):
    schema_factory = ContentSchema

@view_config(name=SliderImage.type_info.add_view, permission='add',
             renderer='kotti:templates/edit/node.pt')
class SliderImageAddform(FileAddForm):
    item_type = _(u'SliderImage')
    item_class = SliderImage

    #def schema_factory(self):
    #    tmpstore = FileUploadTempStore(self.request)
    #    return FileSchema(tmpstore, title_missing=colander.null)


@view_config(name="edit",context=SliderImage,permission="edit",
             renderer = "kotti:templates/edit/node.pt")
class SliderImageEditForm(FileEditForm):
    def schema_factory(self):
        tmpstore = FileUploadTempStore(self.request)
        return FileSchema(tmpstore)

    def edit(self, **appstruct):
        super(SliderImageEditForm,self).edit(**appstruct)
        self.context.link = appstruct['link']


@view_config(name=QuoteRoot.type_info.add_view, permission='add',
             renderer='kotti:templates/edit/node.pt')
class QuoteRootAddform(AddFormView):
    schema_factory = ContentSchema
    add = QuoteRoot
    item_type = _(u'QuoteRoot')


@view_config(name='edit', context=QuoteRoot, permission='edit',
             renderer='kotti:templates/edit/node.pt')
class QuoteRootEditForm(EditFormView):
    schema_factory = ContentSchema

@view_config(name=Quote.type_info.add_view, permission='add',
             renderer='kotti:templates/edit/node.pt')
class QuoteAddform(AddFormView):
    schema_factory = QuoteSchema
    add = Quote
    item_type = _(u'Quote')


@view_config(name='edit', context=Quote, permission='edit',
             renderer='kotti:templates/edit/node.pt')
class QuoteEditForm(EditFormView):
    schema_factory = QuoteSchema

