# -*- coding: utf-8 -*-

"""
Created on 2015-10-20
:author: Anierobi Ephraim (splendidzigy24@gmail.com)
"""

pytest_plugins = "kotti"

from pytest import fixture


@fixture(scope='session')
def custom_settings():
    import kotti_mysite.resources
    kotti_mysite.resources  # make pyflakes happy
    return {
        'kotti.configurators': 'kotti_tinymce.kotti_configure '
                               'kotti_mysite.kotti_configure'}
