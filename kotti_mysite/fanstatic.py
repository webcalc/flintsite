# -*- coding: utf-8 -*-

"""
Created on 2015-03-07
:author:  ()
"""

from __future__ import absolute_import

from fanstatic import Group
from fanstatic import Library
from fanstatic import Resource
from js.bootstrap import bootstrap_css
from kotti.fanstatic import view_needed, edit_needed



library = Library("kotti_mysite", "static")

css = Resource(
    library,
    "styles.css",
    depends=[bootstrap_css,],
    minified="styles.min.css",
    bottom=True)

js = Resource(
    library,
    "scripts.js",
    minified="scripts.min.js",bottom=True)

css_and_js = Group([css, js])
view_needed.add(css_and_js)
edit_needed.add(css_and_js)