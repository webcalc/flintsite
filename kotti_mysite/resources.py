from kotti.interfaces import IDefaultWorkflow
from kotti.resources import Content, Image, Document
from sqlalchemy import Column
from sqlalchemy import ForeignKey
from sqlalchemy import Integer, Unicode
from zope.interface import implements
from kotti.util import _


class Slider(Content):
    implements(IDefaultWorkflow)

    id = Column(Integer, ForeignKey('contents.id'), primary_key=True)
    type_info = Content.type_info.copy(
        name=u'Slider',
        title=_(u'Slider'),
        add_view=u'add_slider',
        addable_to=[u'Document']
    )

class SliderImage(Image):
    id = Column(Integer, ForeignKey("images.id"),primary_key=True)
    link = Column(Unicode(80))
    type_info = Image.type_info.copy(
        name=u"SliderImage",
        title=_(u"Slider Image"),
        add_view = u'add_slider_image',
        addable_to = [u'Slider'],
        selectable_default_views=[],
        uploadable_mimetypes=['image/*', ],
    )

class QuoteRoot(Content):

    implements(IDefaultWorkflow)

    id = Column(Integer,ForeignKey('contents.id'),primary_key=True)
    type_info = Content.type_info.copy(
        name = u"QuoteRoot",
        title = _(u"QuoteRoot"),
        add_view = u"add_QuoteRoot",
        addable_to = [u'Document']
    )

class Quote(Content):
    implements(IDefaultWorkflow)

    id = Column(Integer, ForeignKey("contents.id"), primary_key=True)
    type_info = Content.type_info.copy(
        name = u"Quote",
        title = _(u"Quote"),
        add_view = u"add_Quote",
        addable_to = [u"QuoteRoot"]
    )