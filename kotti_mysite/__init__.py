# -*- coding: utf-8 -*-

"""
Created on 2015-10-20
:author: Anierobi Ephraim (splendidzigy24@gmail.com)
"""

from kotti.resources import Image, Document
from pyramid.i18n import TranslationStringFactory
from kotti_mysite.views.view import related_items
from kotti.views.slots import assign_slot
from kotti_mysite.resources import SliderImage #, Quote
from kotti_mysite.views.edit import SliderImageAddform
_ = TranslationStringFactory('kotti_mysite')


def kotti_configure(settings):
    """ Add a line like this to you .ini file::

            kotti.configurators =
                kotti_mysite.kotti_configure

        to enable the ``kotti_mysite`` add-on.

    :param settings: Kotti configuration dictionary.
    :type settings: dict
    """

    settings['pyramid.includes'] += ' kotti_mysite'
    settings['kotti.available_types'] += (
          ' kotti_mysite.resources.Slider'+
          ' kotti_mysite.resources.SliderImage'+
          ' kotti_mysite.resources.QuoteRoot'+
          ' kotti_mysite.resources.Quote')
    settings['kotti.alembic_dirs'] += ' kotti_mysite:alembic'
    SliderImage.type_info.addable_to.append('Slider')
    Image.type_info.addable_to.append('Blog')



def includeme(config):
    """ Don't add this to your ``pyramid_includes``, but add the
    ``kotti_configure`` above to your ``kotti.configurators`` instead.

    :param config: Pyramid configurator object.
    :type config: :class:`pyramid.config.Configurator`
    """
    #config.add_view(related_items,name="related-items",
    #                renderer="templates/relateditems.pt")
   # assign_slot('related-items','right')
    assign_slot('fbt','belowcontent')
    config.add_translation_dirs('kotti_mysite:locale')
    config.add_static_view('static-kotti_mysite', 'kotti_mysite:static')
    config.scan(__name__)
